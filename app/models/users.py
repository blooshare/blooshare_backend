from app import db
from app.utils.blood_type_enum import blood_type_enum


class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, db.ForeignKey('auth.id'), primary_key=True)
    blood_type = db.Column(db.Enum(blood_type_enum))

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            print(x.blood_type)
            return {
                'id': x.id,
                'blood_type': x.blood_type.value
                }
        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}
