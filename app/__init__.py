from flask import Flask
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager


# Config SQLAlchemi

db = SQLAlchemy()

# JWT
jwt = JWTManager()


def create_app():
    app = Flask(__name__)
    api = Api(app)
    # Alchemy
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SECRET_KEY'] = 'some-secret-string'
    db.init_app(app)
    @app.before_first_request
    def create_tables():
        db.create_all()

    app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
    from app.models.jwtoken import jwToken
    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decrypted_token):
        jti = decrypted_token['jti']
        return jwToken.is_jti_blacklisted(jti)

    jwt.init_app(app)

    from app.resources.auth import user_auth
    from app.resources.users import users

    api.add_resource(user_auth.userRegistration, '/registration')
    api.add_resource(user_auth.userLogin, '/login')
    api.add_resource(user_auth.userLogoutAccess, '/logout/access')
    api.add_resource(user_auth.userLogoutRefresh, '/logout/refresh')
    api.add_resource(user_auth.TokenRefresh, '/token/refresh')
    api.add_resource(users.users, '/users')
    api.add_resource(users.user, '/users/<id>')
    api.add_resource(user_auth.SecretResource, '/secret')
    from app.resources.hospitals import hospitals
    api.add_resource(hospitals.hospitals, '/hospitals')
    return app
