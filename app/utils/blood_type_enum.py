import enum


class blood_type_enum(enum.Enum):
    a_p = "A+"
    a_n = "A-"
    b_p = "B+"
    b_n = "B-"
    o_p = "0+"
    o_n = "0-"
    ab_p = "AB+"
    ab_n = "AB-"
