from flask_restful import Resource, reqparse
from app.models.auth import AuthModel
from app.models.jwtoken import jwToken

from app.utils.custom_hash import customHash
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,
                                get_jwt_identity, get_raw_jwt
                                )

# Comprobamos si los campos email y/o password están vacíos y de ser asi mostramos un mensaje de error.
# Si no estan vacios los campos continuamos con el programa

parser = reqparse.RequestParser()
parser.add_argument(
    'email', help='This field cannot be blank', required=True)
parser.add_argument(
    'password', help='This field cannot be blank', required=True)

# Clase para registrar un usuario


class userRegistration(Resource):
    """ Recoge el usuario y la contraseña que el usuario a introducido
        y comprueba si el usuario existe, de ser así muestra 'Usuario X ya existe'
        Si el usuario no existe creamos un modelo de usuario, lo guardamos en la db
        y crea y devuelve los token de acceso y de refresco.

            Si algo va mal devuelve un 500
    """
    def post(self):
        data = parser.parse_args()

        if AuthModel.find_by_email(data['email']):
            return {'message': 'User {} already exists'. format(data['email'])}

        new_user = AuthModel(
            email=data['email'],
            password=customHash.generate_hash(data['password'])
            )
        try:
            new_user.save_to_db()
            access_token = create_access_token(identity=data['email'])
            refresh_token = create_refresh_token(identity=data['email'])
            return {
                'message': 'User {} was created'.format(data['email']),
                'access_token': access_token,
                'refresh_token': refresh_token
                }
        except Exception:
            return {'message': 'Something went wrong'}, 500

# clase para logear al usuario


class userLogin(Resource):
    """ Recoge el usuario y la contraseña que el usuario a introducido
            y comprueba si el usuario existe, de no ser así muestra 'Usuario X no existe'
            Si el usuario existe nos devuelve el token de acceso y el token refresco.

                Si algo va mal devuelve 'Credenciales equivocadas'

    """
    def post(self):
        data = parser.parse_args()
        current_user = AuthModel.find_by_email(data['email'])
        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['email'])}

        if customHash.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity=data['email'])
            refresh_token = create_refresh_token(identity=data['email'])
            return {
                'message': 'Logged in as {}'.format(current_user.email),
                'access_token': access_token,
                'refresh_token': refresh_token
                }
        else:
            return {'message': 'Wrong credentials'}

# clase cerrar sesión(Token acceso)


class userLogoutAccess(Resource):
    """ Revoca los token de acceso, de dar un error muestra:
            error 500
    """
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = jwToken(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong'}, 500

# clase cerrar sesión(Token acceso)


class userLogoutRefresh(Resource):
    """ Revoca los token de refresco, de dar un error muestra:
            error 500
    """
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = jwToken(jti=jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except Exception as e:
            return {'message': 'Something went wrong:' + e}, 500

# Clase que refresaca el token


class TokenRefresh(Resource):
    """Comprueba la identidad del usuario y rea un nuevo token de acceso"""
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}


class SecretResource(Resource):
    @jwt_required
    def get(self):
        return {
            'answer': 42
            }
