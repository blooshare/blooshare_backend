from flask_restful import Resource, reqparse, request
from app.models.users import UserModel

parser = reqparse.RequestParser()
parser.add_argument(
    'id', help='This field cannot be blank', required=True)
parser.add_argument(
    'blood_type', help='This field cannot be blank', required=True)

parserId = reqparse.RequestParser()
parserId.add_argument(
    'id', help='This field cannot be blank', required=True)


class users(Resource):
    def get(self):
        return UserModel.return_all()

    def post(self):
        data = parser.parse_args()
        if UserModel.find_by_id(data['id']):
            return {'message': 'User {} already exists'. format(data['id'])}

        new_user = UserModel(id=data['id'], blood_type=data['blood_type'])

        try:
            new_user.save_to_db()
            return {
                'message': 'User {} was created'.format(data['id']),
                'pesado': 'Blood Type: {}'.format(data['blood_type'])
                }
        except Exception as e:
            print(e)
            return {'message': 'Something went wrong '}, 500


class user(Resource):
    def get(self, id):
        #id = request.json['id']
        user = UserModel.find_by_id(id)
        if user:
            return {'data': {'blood': user.blood_type.value, 'id': user.id}}

        return {'message': 'Solo un user'}

    def put(self):
        return {'message': 'Update user'}

    def patch(self):
        return {'message': 'Update 2 user'}

    def delete(self):
        return {'message': 'delete user'}
