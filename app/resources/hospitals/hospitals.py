from flask_restful import Resource, reqparse
from app.models.jwtoken import jwToken

from app.utils.custom_hash import customHash
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)


class hospitals(Resource):
    """ Recoge el usuario y la contraseña que el usuario a introducido
            y comprueba si el usuario existe, de no ser así muestra 'Usuario X no existe'
            Si el usuario existe nos devuelve el token de acceso y el token refresco.

                Si algo va mal devuelve 'Credenciales equivocadas'

    """
    def get(self):
        return [
            {
                'name': 'Hospital Las Rozas',
                },
                {'name': 'Hospital Puerta de Hierro'}
            ]
